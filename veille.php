<!DOCTYPE html>
<html lang="en">
<head>
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

<div class="container">
        <div class="row">
        
        <?php 
            include 'php/db_connection.php';
            $query = "SELECT * FROM `veilles`";
            $sth = $bdd->prepare($query);
            $sth->execute();
            $results = $sth->fetchAll(PDO::FETCH_ASSOC);
        ?>

        <?php
            foreach($results as $veille){ 
        ?>
        
        <div class="col-4">
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="img/<?php echo $veille['image']; ?>" alt="Card image cap">
            
                <div class="card-body">
                    <h5 class="card-title"><?php echo $veille['sujet']; ?></h5>
                    <p class="card-text"><?php echo $veille['lien']; ?></p>
                    <a href="#" class="btn btn-primary">EDITER</a>
                    <a href="#" class="btn btn-danger">SUPPRIMER</a>
                </div>
            </div>
        </div>
        
        <?php
            }
        ?>
    </div>
</div>
    
</body>
</html>



