<?php include "php/db_connection.php"; ?>  
<?php include 'php/isset_login.php';?>

<script>
  localStorage.setItem("id", <?php $result["id"]; ?>);
  location.href = "signup.php";
</script>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="keywords" content="">

    <title>TheSaaS - Login</title>

    <!-- BootStrap CSS JS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

    <!-- Styles -->
    <link href="assets/css/core.min.css" rel="stylesheet">
    <link href="assets/css/thesaas.min.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">

    <!-- Favicons -->
    <link rel="apple-touch-icon" href="assets/img/apple-touch-icon.png">
    <link rel="icon" href="assets/img/favicon.png">

    <!-- JQuery -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>

  <body class="mh-fullscreen bg-img center-vh p-20" style="background-image: url(assets/img/bg-girl.jpg);">
   <div class="card card-shadowed p-50 w-400 mb-0" style="max-width: 100%">
      <h5 class="text-uppercase text-center">Connexion</h5>
      <br><br>

      <form method="POST" id="login-form" class="login-form text-center">
        <div class="form-group">
          <input id="email" type="text" name="username" class="form-control" placeholder="Username">
        </div>
       <div class="form-group">
          <input id="password" type="password" name="password" class="form-control" placeholder="Password">
        </div>
       <div class="form-group flexbox py-10">
          <label class="custom-control custom-checkbox">
            <input type="checkbox" name="checkbox" class="custom-control-input" checked>
            <span class="custom-control-indicator"></span>
            <span class="custom-control-description">Remember me</span>
          </label>
          <a class="text-muted hover-primary fs-13" href="#">Forgot password?</a>
        </div>
        <div class="form-group">
          <button class="btn btn-bold btn-block btn-primary" type="submit">Login</button>
          <input type="hidden" name="login" value="1">
        </div>
      </form>

      <div class="divider">Or Sign In With</div>
      <div class="text-center">
        <a class="btn btn-circular btn-sm btn-facebook mr-4" href="#"><i class="fa fa-facebook"></i></a>
        <a class="btn btn-circular btn-sm btn-google mr-4" href="#"><i class="fa fa-google"></i></a>
        <a class="btn btn-circular btn-sm btn-twitter" href="#"><i class="fa fa-twitter"></i></a>
      </div>

      <hr class="w-30">

      <p class="text-center text-muted fs-13 mt-20">Don't have an account? <a href="page-register.html">Sign up</a></p>
    </div>
    <!-- Scripts -->
    <script src="assets/js/core.min.js"></script>
    <script src="assets/js/thesaas.min.js"></script>
    <script src="assets/js/script.js"></script>
    <script src="js/main.js"></script>
    <script src="js/airtable.browser.js"></script>
    <script src="config.js"></script>
 </body>
</html>
