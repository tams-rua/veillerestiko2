import React from 'react'

const test = ({ test, updates }) => {
    return (
      <StyledGoal>
        <StyledCheckBox>
          {" "}
          <input type="checkbox" defaultChecked={test.fields.complete} disabled />
          <span />
        </StyledCheckBox>
        <h2>{test.fields.title}</h2>
        <StyledtestDetails>
          <h3>DETAILS</h3>
          <p>{test.fields.details}</p>
          <h3>UPDATES</h3>
          {updates.map((update, index) => (
            <p key={index}>{update.fields.update}</p>
          ))}
        </StyledTestDetails>
      </StyledGoal>
    );
  };

export default test;