<?php include 'php/db_connection.php';?>


<!DOCTYPE html>
<html lang="en">
  <head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css">
    <link rel="stylesheet" type="text/css" href="style.css">
   
    <title>Animara Matcher - profil</title>
  </head>
<?php include "php/navbar.php"?>
<script>
  $(document).ready(function(){

    $.ajax({
        url : 'php/profil.php?id=' + localStorage.getItem("id"),
        type : 'GET',
        success : function(data, statut){ // code_html contient le HTML renvoyé
          var myInfos = JSON.parse(data);
            console.log("myInfos", myInfos);
        }
     }); 
     $.ajax({
        url : 'php/myAnimals.php?id=' + localStorage.getItem("id"),
        type : 'GET',
        success : function(data, statut){ // code_html contient le HTML renvoyé
          var myAnimals = JSON.parse(data);
            console.log("myAnimals", myAnimals);

            myAnimals.forEach(animal => {
              var newAnimal = 
                '<div class="col-12 col-sm-6 col-lg-6">' +
                  '<div class="animal position-relative card mb-3">' +
                    '<div class="row g-0">' +
                      '<div class="col-md-12">' +
                        '<img src="img/###IMAGE###" class="img-fluid w-100 pt-3" alt="###NOM###" >' +
                      '</div>' +
                    '</div>' +
                    '<div class="content">' +
                      '<div class="row">' +
                        '<div class="col-md-12">' +
                          '<div class="card-body">' +
                            '<h5 class="card-title">###NOM###</h5>' +
                            '<p class="card-text">###DESCRIPTION###</p>' +
                            '<a class="btn btn-warning">Modifier</a>' +
                            '<a class="btn btn-danger">Supprimer</a>' +
                          '</div>' +
                        '</div>' +
                      '</div>' +
                    '</div>' +
                  '</div>' +
                '</div>';
console.log("newAnimal", newAnimal);

                var myAnimal = newAnimal.replaceAll("###IMAGE###", animal.img_animal);
                myAnimal = myAnimal.replaceAll("###NOM###", animal.name);
                myAnimal = myAnimal.replaceAll("###DESCRIPTION###", animal.description);

                $("#myAnimals").append(myAnimal);
            })
        }
     }); 

     
  })
</script>
<body>
  

    <div class="banner col-12 col-6">
        <div class="mt-5 pb-2">
           
            <div class="container">
                <h2 class=""></h2>
                <div class="card">
                  <div class="card-body">
                     This is some text within a card body.
                  </div>
                </div> 
            </div>

            <div class="container">
                <h2 class="pt-2 pb-2">Profil animal</h2>
              <div id="myAnimals" class="row">

                <div class="col-12 col-sm-6 col-lg-6">
                  <div class="animal position-relative card mb-3">
                    <div class="row g-0">
                      <div class="col-md-12">
                        <div class="addAnimal"><a href="form.php">+</a></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>  
            </div>
          </div>
        </div>

</section>
</body>
</html>