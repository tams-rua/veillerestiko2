<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="keywords" content="">

    <title>TheSaaS - Register</title>

    <!-- Styles -->
    <link href="assets/css/core.min.css" rel="stylesheet">
    <link href="assets/css/thesaas.min.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">

    <!-- Favicons -->
    <link rel="apple-touch-icon" href="assets/img/apple-touch-icon.png">
    <link rel="icon" href="assets/img/favicon.png">

    <!-- Script Inscription JS-->
    <script src="js/main.js"></script>
    
    <script>
      var ajaxurl = '/';
    </script>
  </head>

  <body class="mh-fullscreen bg-img center-vh p-20" style="background-image: url(assets/img/bg-girl.jpg);">
    <div class="card card-shadowed p-50 w-400 mb-0" style="max-width: 100%">
      <h5 class="text-uppercase text-center">Inscription</h5>
      <br><br>

      <form class="form-type-material">
        <div class="form-group">
          <input id="user" type="text" class="form-control" placeholder="Nom Prénom">
        </div>

        <div class="form-group">
          <input id="nameUser" type="text" class="form-control" placeholder="Utilisateur">
        </div>

        <div class="form-group">
          <input id="emailUser" type="text" class="form-control" placeholder="Email">
        </div>

        <div class="form-group">
          <input id="passwordUser" type="password" class="form-control" placeholder="Mot de Passe">
        </div>
        <br>
        <button onclick="myInscription()" class="btn btn-bold btn-block btn-primary" type="submit">S'inscrire</button>
      </form>

      <hr class="w-30">

      <p class="text-center text-muted fs-13 mt-20">Already have an account? <a href="page-login.html">Sign in</a></p>
    </div>




    <!-- Scripts -->
    <script src="assets/js/core.min.js"></script>
    <script src="assets/js/thesaas.min.js"></script>
    <script src="js/airtable.browser.js"></script>
    <script src="js/config.js"></script>
    

  </body>
</html>
